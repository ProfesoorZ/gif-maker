#include "projectData.h"
#include "view.h"

/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
*/
void myFgets(char str[], int n)
{
	fgets(str, n, stdin);
	str[strcspn(str, "\n")] = 0;
}

/*
This function prints to the user all of the option are in the program and gets his choice
input: none
output: the choice of the user
*/
int getChoice()
{
	int choice = 0;
	do
	{
		printf("What would you like to do?\n");
		printf("[0] Exit\n");
		printf("[1] Add new frame\n");
		printf("[2] Remove a frame\n");
		printf("[3] Change a frame index\n");
		printf("[4] Change frame duration\n");
		printf("[5] Change duration of all frames\n");
		printf("[6] List frames\n");
		printf("[7] Play movie!\n");
		printf("[8] Save Project\n");
		(void)scanf("%d", &choice);
		(void)getchar();
	} while (choice < EXIT || choice > SAVE_PROJECT);
	return choice;
}

/*
This function gets a name of a file and checks if its valid
input: the file name
output: 1 if the file is valid 0 otherwise
*/
int isFileExist(char* fileName)
{
	IplImage* image;
	image = cvLoadImage(fileName, 1);
	if (image)
	{
		cvReleaseImage(&image);
		return 1;
	}
	return 0;
}

/*
This function checks if the number of the frame rate is valid
input: the number to check
output: the valid number
*/
int checkValidFrameRate(int check)
{
	int tmpDur = check;
	while (tmpDur < 1)
	{
		printf("The duration of the frame cannot be below zero, type another duration\n");
		(void)scanf("%d", &tmpDur);
		(void)getchar();
	}
	return tmpDur;
}

/*
This function gets the open project option from the user
input: none
output: the choice from the user
*/
int getOpenOption()
{
	int op = 0;
	do
	{
		printf("[0] create a new project\n");
		printf("[1] Load existing project\n");
		(void)scanf("%d", &op);
		(void)getchar();
	} while (op < 0 || op > 1);
	return op;
}

/*
This function checks if the path of the project is txt and exist
input: the path
output: 1 if valid, 0 otherwise
*/
int checkProjectFile(char* path)
{
	FILE* file;
	file = fopen(path, "r");
	if (file && strstr(path, ".txt"))
	{
		return 1;
	}
	return 0;
}

/*
This function open the project from a file
input: FrameNode pointer for the head and the file path
output: the FrameNode* head
*/
FrameNode* openProject(FrameNode* head, char* path)
{
	FILE* file = NULL;
	Frame* newFrame = NULL;
	char currPath[STR_LEN] = { 0 }, name[STR_LEN] = { 0 }, line[STR_LEN] = { 0 };
	unsigned int dur = 0;

	while (!checkProjectFile(path))
	{
		printf("Please enter the project path, end with .txt\n");
		myFgets(path, STR_LEN);
	}
	file = fopen(path, "r");

	while (fgets(line, STR_LEN, file) != NULL)	//reading each line
	{
		(void)sscanf(line, "%s %s %u\n", currPath, name, &dur);	//putting it in the different variables
		newFrame = createFrame(name, dur, currPath);	//creating the new frame
		addFrame(&head, newFrame);	//adding it to the list
	}
	fclose(file);
	return head;	//returning the list head
}

/*
This function save the project to a file
input: the head of the list and the file path
output: none
*/
void saveProject(FrameNode* head, char* path)
{
	FrameNode* curr = head;
	FILE* file;
	while (!strstr(path, ".txt"))	//if the file path doesnt contain the txt ending
	{
		printf("Please enter the project path, end with .txt\n");
		myFgets(path, STR_LEN);
	}
	file = fopen(path, "w");
	while (curr)
	{
		fprintf(file, "%s %s %u\n", curr->frame->path, curr->frame->name, curr->frame->duration);	//writing to the file by the format
		curr = curr->next;
	}
	fclose(file);
}