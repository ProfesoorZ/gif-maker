#include "LinkedList.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

/*
This fcuntion adds to the head of the list a new frame
input: the pointer to the head and a pointer to the new frame
output: none
*/
void addFrame(FrameNode** head, Frame* newFrame)
{
	FrameNode* curr = *head;
	FrameNode* newNode = (FrameNode*)malloc(sizeof(FrameNode));
	assert(newNode);

	newNode->frame = newFrame;
	newNode->next = NULL;

	if (!curr)	//if the list has zero nodes
	{
		*head = newNode;
	}
	else
	{
		while (curr->next)
		{
			curr = curr->next;
		}
		curr->next = newNode;
	}
}

/*
This function creates a new Frame with given parameteres
input: the new frame-name, path and frame rate
output: a pointer to the new Frame
*/
Frame* createFrame(char* name, int len, char* path)
{
	Frame* newFrame = (Frame*)malloc(sizeof(Frame));
	assert(newFrame);

	newFrame->duration = len;

	newFrame->name = (char*)malloc(sizeof(char) * strlen(name) + 1);	//plus one for the null 
	assert(newFrame->name);
	strncpy(newFrame->name, name, strlen(name));
	newFrame->name[strlen(name)] = '\0';

	newFrame->path = (char*)malloc(sizeof(char) * strlen(path) + 1);	//plus one for the null 
	assert(newFrame->path);
	strncpy(newFrame->path, path, strlen(path));
	newFrame->path[strlen(path)] = '\0';

	return newFrame;
}

/*
This function prints the data of each FrameNode in the list
input: the head of the list
output: none
*/
void showFrames(FrameNode* head)
{
	FrameNode* curr = head;
	printf("\t\tName\t\tDuration\t\tPath\n");
	while (curr)
	{
		printf("\t\t%s\t\t%u ms\t\t%s\n", curr->frame->name, curr->frame->duration, curr->frame->path);
		curr = curr->next;
	}
}

/*
This function frees all of the data in the list of the frames
input: the head of the list
output: none
*/
void freeVideo(FrameNode* head)
{
	if (!head)	//if the head is null
	{
		head = NULL;
	}
	else
	{
		FrameNode* curr = head;
		FrameNode* tmp = NULL;
		while (curr)
		{
			free(curr->frame->name);
			free(curr->frame->path);
			free(curr->frame);
			tmp = curr->next;
			free(curr);
			curr = tmp;
		}
		head = NULL;
	}
}

/*
This function deletes a frame from the list
input: the head of the list and the name of the frame you want to delete
output: none
*/
void deleteFrame(FrameNode** head, char* frameName)
{
	FrameNode* curr = *head;
	FrameNode* dNode = NULL;
	if (curr)	//if the head has nodes
	{
		if (!searchFrame(*head, frameName)) //if the frame name is not in the list
		{
			printf("The frame ~%s~ is not in the list\n", frameName);
		}
		else if (!strncmp(curr->frame->name, frameName, strlen(frameName)))	//remove the head of the list
		{
			dNode = curr;
			*head = curr->next;
			free(dNode->frame->name);
			free(dNode->frame->path);
			free(dNode->frame);
			free(dNode);
			dNode = NULL;
			printf("The frame ~%s~ was removed\n", frameName);
		}
		else	//removing any othe part of the list
		{
			while (curr->next && strncmp(frameName, curr->next->frame->name, strlen(frameName)))
			{
				curr = curr->next;
			}

			if (curr->next && !strncmp(frameName, curr->next->frame->name, strlen(frameName)))
			{
				dNode = curr->next;
				curr->next = dNode->next;
				free(dNode->frame->name);
				free(dNode->frame->path);
				free(dNode->frame);
				free(dNode);
				dNode = NULL;
				printf("The frame ~%s~ was removed\n", frameName);
			}
		}
	}
}

/*
This function changes the frame rate for all of the frames in the list
input: the head of the list and the new frame rate to be set
output: none
*/
void changeAllFrameRate(FrameNode* head, int newDur)
{
	FrameNode* curr = head;
	while (curr)
	{
		curr->frame->duration = newDur;
		curr = curr->next;
	}
}

/*
This function changes to one frame its frame rate
input: the head of the list, the frame name which wanted to be change and the new frame rate to be set
output: none
*/
void changeFrameRate(FrameNode* head, char* frameName, int newDur)
{
	FrameNode* curr = head;
	while (curr && strncmp(frameName, curr->frame->name, strlen(frameName)))	//going to the frame which supposed to be changed
	{
		curr = curr->next;
	}
	if (curr)	//if the frame is exist
	{
		curr->frame->duration = newDur;
		printf("Frame ~%s~ frame rate was change to %u\n", frameName, newDur);
	}
	else
	{
		printf("Frame ~%s~ not in the list\n", frameName);
	}
}

/*
This function searches a frame in the list
input: the head of the list and the frame name which wanted to be searched
output: 1 if the frame exist, 0 otherwise
*/
int searchFrame(FrameNode* head, char* frameName)
{
	FrameNode* curr = head;
	int isFound = 0;
	while (curr && !isFound)
	{
		if (!strncmp(frameName, curr->frame->name, strlen(frameName)))
		{
			isFound = 1;
		}
		curr = curr->next;
	}
	return isFound;
}

/*
This function returns the size of the list
input: the head of the list
output: the length of the list
*/
int getListLength(FrameNode* head)
{
	FrameNode* curr = head;
	int len = 0;
	while (curr)
	{
		len++;
		curr = curr->next;
	}
	return len;
}

/*
This function moves a frame to a new index
input: the head of the list, the frame name and the new index wanted to be moved to
output: none
*/
void moveFrame(FrameNode** head, char* frameName, int newIndex)
{
	if (*head)
	{
		if (newIndex > getListLength(*head) || newIndex < 1)	//invalid new index
		{
			printf("You entered an invalid index\n");
		}
		else if (!searchFrame(*head, frameName))		//the frame name is not exist in the list
		{
			printf("The frame name that you typed not exist in the list\n");
		}
		else if (getListLength(*head) == 1)		//the list has only one node
		{
			printf("Move successfully done\n");
		}
		else
		{
			int index = 1, indexForChecking = 1;
			FrameNode* curr = *head;
			FrameNode* newPlace = *head;
			FrameNode* tmp = NULL;
			while (strncmp(curr->frame->name, frameName, strlen(frameName)))
			{
				indexForChecking++;
				curr = curr->next;
			}
			curr = *head;
			if (indexForChecking == newIndex)	//if the frame wanted to be moves is already on the index wanted to be moved to
			{
				printf("Move successfully done\n");
			}
			else if(newIndex == 1)	//if you want to move to the head of the list
			{
				while (strncmp(curr->next->frame->name, frameName, strlen(frameName)))
				{
					curr = curr->next;
				}
				tmp = curr->next->next;
				curr->next->next = *head;
				*head = curr->next;
				curr->next = tmp;
				printf("Move successfully done\n");
			}
			else if (!strncmp(curr->frame->name, frameName, strlen(frameName)))	//if you want to move the head of the list
			{
				*head = curr->next;
				for (index = 1; index < newIndex - 1; index++)
				{
					newPlace = newPlace->next;
				}
				tmp = newPlace->next->next;
				newPlace->next->next = curr;
				curr->next = tmp;
				printf("Move successfully done\n");
			}
			else	// if you want to move any other part of the list
			{
				while (strncmp(frameName, curr->next->frame->name, strlen(frameName)))
				{
					curr = curr->next;
				}
				tmp = curr->next;
				curr->next = curr->next->next;
				for (index = 1; index < newIndex-1; index++)
				{
					newPlace = newPlace->next;
				}
				tmp->next = newPlace->next;
				newPlace->next = tmp;
				printf("Move successfully done\n");
			}
		}
	}
}