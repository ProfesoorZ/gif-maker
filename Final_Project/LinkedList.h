#ifndef LINKEDLISTH
#define LINKEDLISTH

#define FALSE 0
#define TRUE !FALSE

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

// Frame struct
typedef struct Frame
{
	char* name;
	unsigned int duration;
	char* path;
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

void addFrame(FrameNode** head, Frame* newNode);
Frame* createFrame(char* name, int len, char* path);
void showFrames(FrameNode* head);
void freeVideo(FrameNode* head);
void deleteFrame(FrameNode** head, char* frameName);
void changeAllFrameRate(FrameNode* head, int newDur);
void changeFrameRate(FrameNode* head, char* frameName, int newDur);
int searchFrame(FrameNode* head, char* frameName);
int getListLength(FrameNode* head);
void moveFrame(FrameNode** head, char* frameName, int newIndex);

#endif
