#ifndef PROJECTDATA

#include "LinkedList.h"
#define EXIT 0
#define SAVE_PROJECT 8
#define PLAY_MOVIE 7
#define STR_LEN 50

void myFgets(char str[], int n);
int getChoice();
int isFileExist(char* fileName);
int checkValidFrameRate(int check);
int getOpenOption();
int checkProjectFile(char* path);
FrameNode* openProject(FrameNode* head, char* path);
void saveProject(FrameNode* head, char* path);

#endif // !PROJECTDATA

