/*********************************
* Class: MAGSHIMIM C2			 *
* openCV template      			 *
**********************************/

#include "view.h"
#include "projectData.h"

#define _CRTDBG_MAP_ALLOC
#define STR_LEN 50
#define EXIT 0
#define ADD_FRAME 1
#define DELETE_FRAME 2
#define MOVE_FRAME 3
#define CHANGE_ONE_FRAME_RATE 4
#define CHANGE_ALL_FRAME_RATE 5
#define PRINT_FRAMES 6
#define PLAY_MOVIE 7

int main(void)
{
	FrameNode* head = NULL;
	Frame* newFrame = NULL;
	char tmpName[STR_LEN] = { 0 }, tmpPath[STR_LEN] = { 0 };
	int tmpDur = 0, choice = 1, newIndex = 0, openChoice = 0;

	printf("\nHello! welcome to my movie maker\n");

	openChoice = getOpenOption();

	if (openChoice)
	{
		printf("Enter the path of the project, include its name:\n");
		myFgets(tmpPath, STR_LEN);
		head = openProject(head, tmpPath);
	}

	while (choice)
	{
		choice = getChoice();

		switch (choice)
		{
		case ADD_FRAME:
			memset(tmpName, 0, STR_LEN);		//if the new name will be longer than the last name
			memset(tmpPath, 0, STR_LEN);		//if the new path will be longer than the last path
			printf("creating a new frame\n");
			printf("Enter the frame path\n");
			myFgets(tmpPath, STR_LEN);
			if (isFileExist(tmpPath))
			{
				printf("Enter frame length (in milliseconds)\n");
				(void)scanf("%u", &tmpDur);
				(void)getchar();
				tmpDur = checkValidFrameRate(tmpDur);
				printf("Enter the frame name\n");
				myFgets(tmpName, STR_LEN);
				while (searchFrame(head, tmpName))
				{
					printf("This name is already taken, please enter another name\n");
					memset(tmpName, 0, STR_LEN);
					myFgets(tmpName, STR_LEN);
				}
				newFrame = createFrame(tmpName, tmpDur, tmpPath);
				addFrame(&head, newFrame);
			}
			else
			{
				printf("Unable to create frame, the path is wrong or the file isn't a picture\n");
			}
			break;

		case DELETE_FRAME:
			memset(tmpName, 0, STR_LEN);	//if any letters remained in the last temporary name
			printf("Enter the frame name you want to erase\n");
			myFgets(tmpName, STR_LEN);
			deleteFrame(&head, tmpName);
			break;

		case MOVE_FRAME:
			memset(tmpName, 0, STR_LEN);	//if any letters remained in the last temporary name
			printf("Enter the frame name you would like to move\n");
			myFgets(tmpName, STR_LEN);
			printf("Enter the index you would like to move the frame ~%s~ to\n", tmpName);
			(void)scanf("%d", &newIndex);
			(void)getchar();
			moveFrame(&head, tmpName, newIndex);
			break;

		case CHANGE_ONE_FRAME_RATE:
			memset(tmpName, 0, STR_LEN);	//if any letters remained in the last temporary name
			printf("Enter the frame name you want to chage its frame rate\n");
			myFgets(tmpName, STR_LEN);
			printf("Enter the new frame rate you want to put in the frame %s\n", tmpName);
			(void)scanf("%d", &tmpDur);
			(void)getchar();
			tmpDur = checkValidFrameRate(tmpDur);
			changeFrameRate(head, tmpName, tmpDur);
			break;

		case CHANGE_ALL_FRAME_RATE:
			printf("Enter the new frame rate for all the frames\n");
			(void)scanf("%d", &tmpDur);
			(void)getchar();
			tmpDur = checkValidFrameRate(tmpDur);
			changeAllFrameRate(head, tmpDur);
			break;

		case PRINT_FRAMES:
			showFrames(head);
			break;

		case PLAY_MOVIE:
			play(head);
			break;

		case SAVE_PROJECT:
			memset(tmpPath, 0, STR_LEN);
			printf("Enter the path of the project, include its name:\n");
			myFgets(tmpPath, STR_LEN);
			saveProject(head, tmpPath);
			break;
		}
	}
	printf("Goodbye!\n");

	freeVideo(head);
	return 0;
}